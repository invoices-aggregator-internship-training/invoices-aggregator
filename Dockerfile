FROM maven:3-jdk-8-alpine
WORKDIR /usr/src/app
COPY target/demo-0.0.1-SNAPSHOT.jar demo.jar
CMD [ "sh", "-c", "java -jar demo.jar" ]
