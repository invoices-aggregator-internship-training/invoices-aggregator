package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class DemoApplication {

	@Value("${invoices.mobile-carriers.url}")
	private String mobileCarriersInvoiceServiceUrl;

	@GetMapping("invoice")
	public Invoice getAllInvoicesValue(@RequestParam String cnp) {
		return getInvoiceForCNP(cnp);
	}

	private Invoice getInvoiceForCNP(String cnp) {
		Invoice invoice = new Invoice();
		invoice.name = getNameByCNP(cnp);
		invoice.invoicesTotalValue += getMobileCarriersInvoicesValue(cnp);
		return invoice;
	}

	private int getMobileCarriersInvoicesValue(String cnp) {
		MobileCarriersInvoice invoice = callMobileCarriersInvoiceService(cnp);
		return invoice.telekomValue + invoice.vodafoneValue;
	}

	private MobileCarriersInvoice callMobileCarriersInvoiceService(String cnp) {
		RestTemplate restTemplate = new RestTemplate();
		String url = mobileCarriersInvoiceServiceUrl + cnp;
		return restTemplate.getForEntity(url, MobileCarriersInvoice.class).getBody();
	}

	private String getNameByCNP(String cnp) {
		return "Stanciu Marian";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}